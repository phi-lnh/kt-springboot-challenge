package kt.com.example.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
 
import kt.com.example.entity.Gpx;
 
@Repository
public interface GpxRepository extends JpaRepository<Gpx,Long>{
 
}