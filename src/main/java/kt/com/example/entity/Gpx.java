package kt.com.example.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
 
@Entity
@Table(name="GPX2")
public class Gpx {
  
 @Id
 @GeneratedValue(strategy= GenerationType.IDENTITY)
 private Long id;
  
 @Column(name="GPX_NAME")
 private String name;
  
 @Column(name="GPX_DESCRIPTION")
 private String description;
 
 
 public Long getId() {
  return id;
 }
 
 public void setId(Long id) {
  this.id = id;
 }
 
 public String getName() {
  return name;
 }
 
 public void setName(String name) {
  this.name = name;
 }
 
 public String getDescription() {
  return description;
 }
 
 public void setDescription(String description) {
  this.description = description;
 }
 
}