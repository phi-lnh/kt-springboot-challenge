package kt.com.example.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import kt.com.example.entity.Gpx;
import kt.com.example.service.GpxService;

@RestController
public class GpxController {
  
 @Autowired
 private GpxService gpxService;
  
 public void setGpxService(GpxService gpxService) {
  this.gpxService = gpxService;
 }
 
 @GetMapping("/api/gpxs")
 public List<Gpx> getGpxs() {
  List<Gpx> gpxs = gpxService.retrieveGpxs();
  return gpxs;
 }
  
 @GetMapping("/api/gpxs/{GpxId}")
 public Gpx getGpx(@PathVariable(name="gpxId")Long gpxId) {
  return gpxService.getGpx(gpxId);
 }
  
 @PostMapping("/api/gpxs")
 public void saveGpx(Gpx gpx){
	 gpxService.saveGpx(gpx);
  System.out.println("Gpx Saved Successfully");
 }
  
 @DeleteMapping("/api/gpxs/{gpxId}")
 public void deleteGpx(@PathVariable(name="gpxId")Long gpxId){
	 gpxService.deleteGpx(gpxId);
  System.out.println("Gpx Deleted Successfully");
 }
  
 @PutMapping("/api/gpxs/{gpxId}")
 public void updateGpx(@RequestBody Gpx gpx,
   @PathVariable(name="gpxId")Long gpxId){
  Gpx gpxx = gpxService.getGpx(gpxId);
  if(gpxx != null){
	  gpxService.updateGpx(gpx);
  }
   
 }
 
}