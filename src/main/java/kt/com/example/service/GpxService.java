package kt.com.example.service;
import java.util.List;
import kt.com.example.entity.Gpx;

public interface GpxService {
 public List<Gpx> retrieveGpxs();
  
 public Gpx getGpx(Long gpxId);
  
 public void saveGpx(Gpx gpx);
  
 public void deleteGpx(Long gpxId);
  
 public void updateGpx(Gpx gpx);
}