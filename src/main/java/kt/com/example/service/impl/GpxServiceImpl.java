package kt.com.example.service.impl;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import kt.com.example.entity.Gpx;
import kt.com.example.repository.GpxRepository;
import kt.com.example.service.GpxService;
 
@Service
public class GpxServiceImpl implements GpxService{
 
 @Autowired
 private GpxRepository gpxRepository;
 
 public void setEmployeeRepository(GpxRepository gpxRepository) {
  this.gpxRepository = gpxRepository;
 }
  
 public List<Gpx> retrieveGpxs() {
  List<Gpx> gpxs = gpxRepository.findAll();
  return gpxs;
 }
  
 public Gpx getGpx(Long gpxId) {
  Optional<Gpx> optEmp = gpxRepository.findById(gpxId);
  return optEmp.get();
 }
  
 public void saveGpx(Gpx gpx){
	 gpxRepository.save(gpx);
 }
  
 public void deleteGpx(Long gpxId){
	 gpxRepository.deleteById(gpxId);
 }
  
 public void updateGpx(Gpx gpx) {
	 gpxRepository.save(gpx);
 }
}